package com.quequiere.immobot;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import com.quequiere.immobot.object.Profile;
import com.quequiere.immobot.runnable.BanqueRunnable;
import com.quequiere.immobot.runnable.EnchereRunnable;

public class Immobot
{

	public static Profile p;

	public static long timeDiffServer = 0;

	public static void main(String... args)
	{
		System.out.println("Starting bot ....");
		// -user name -psw pass -money 100 -scan 30

		if (args.length < 4)
		{
			System.out.println("Use:");
			System.out.println("-user username");
			System.out.println("-psw password");
			System.out.println("-money minMoney (fac)");
			System.out.println("-scan minuteScan (fac)");
			System.out.println("-coef minimumcoeftoSell (fac)");
			return;

		}

		String user = "";
		String psw = "";

		for (int x = 0; x < args.length; x++)
		{
			String current = args[x];
			String next = "";
			if (x + 1 != args.length)
			{
				next = args[x + 1];
			}

			switch (current) {
			case "-user":
				user = next;
				break;
			case "-psw":
				psw = next;
				break;
			case "-money":
				EnchereRunnable.minMoney = Long.parseLong(next);
				System.out.println("Min money set to: "+EnchereRunnable.minMoney);
				break;
			case "-scan":
				EnchereRunnable.scanMin = Long.parseLong(next);
				System.out.println("Scan min set to: "+EnchereRunnable.scanMin);
				break;
			case "-coef":
				EnchereRunnable.minCoef = Integer.parseInt(next);
				System.out.println("Coef min set to: "+EnchereRunnable.minCoef);
				break;
			}

		}

		if (user.equals("") || psw.equals(""))
		{
			System.out.println("You need to give user and password !");
			return;
		}


		p = new Profile(user, psw, false);
		p.connect();

		//synchroHorloge();

		p.loadBatiment();

		//launcherEnchere();
		
		
		//launcherBanque();

		//launcherEnchere();
		// p.printLink();

		 Calculator.calculBeneficePerDay();
		 Calculator.printEvolution();

		// Calculator.printConstruction();
		 //Calculator.printLocation();
		 //CalculatorEnchere.printDeal();

	}

	public static void synchroHorloge()
	{

		String TIME_SERVER = "monde2.empireimmo.com";

		try
		{
			NTPUDPClient timeClient = new NTPUDPClient();

			InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
			TimeInfo timeInfo = timeClient.getTime(inetAddress);
			long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
			Date time = new Date(returnTime);

			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");

			String dns = sdf.format(time);

			System.out.println("servertime: " + dns);

			timeDiffServer = time.getTime() - new Date().getTime() - 2000;

			System.out.println("Time diff: " + timeDiffServer);

		}
		catch (UnknownHostException e)
		{

			e.printStackTrace();
		}
		catch (IOException e)
		{

			e.printStackTrace();
		}

	}
	
	public static String cleanString(String s)
	{
		String s2 = s.replace(" ", "");
		s2 = s2.replace("\r", "");
		s2 = s2.replace("\n", "");
		s2 = s2.replace("\t", "");
		return s2;
	}


	static public String sansAccents(String s)
	{

		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[^\\p{ASCII}]", "");
		return s;
	}

	public static void launcherEnchere()
	{
		Thread t = new Thread(new EnchereRunnable());
		t.start();
	}
	
	public static void launcherBanque()
	{
		Thread t = new Thread(new BanqueRunnable());
		t.start();
	}

}
