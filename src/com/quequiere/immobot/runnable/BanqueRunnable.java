package com.quequiere.immobot.runnable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.quequiere.immobot.Immobot;

public class BanqueRunnable implements Runnable
{

	public static boolean errored = false;

	@Override
	public void run()
	{

		while (true)
		{

			try
			{
				Thread.sleep(250);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			long now = System.currentTimeMillis() + Immobot.timeDiffServer;

			long diff = (EnchereRunnable.nextScan - now) / 1000;

			int day = (int) TimeUnit.SECONDS.toDays(diff);
			long hours = TimeUnit.SECONDS.toHours(diff) - (day * 24);
			long minute = TimeUnit.SECONDS.toMinutes(diff) - (TimeUnit.SECONDS.toHours(diff) * 60);
			long second = TimeUnit.SECONDS.toSeconds(diff) - (TimeUnit.SECONDS.toMinutes(diff) * 60);

			String s = day + "d " + hours + ":" + minute + ":" + second;

			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
			Date dn = new Date(System.currentTimeMillis() + Immobot.timeDiffServer);
			String dns = sdf.format(dn);

			System.out.print("\r" + s + "  current: " + dns + " Banque mod");

			System.out.flush();

			if (now >= EnchereRunnable.nextScan && !errored)
			{

				System.out.println("=== Starting new banque ===");
				Immobot.p.connect();

				Random r = new Random();
				int Low = 80;
				int High = 120;
				long randomNum = r.nextInt(High - Low) + Low;
				System.out.println("Random res: " + randomNum);

				double ratioChange = randomNum / 100.0;
				System.out.println("Random res: " + ratioChange);

				double nextok = ((EnchereRunnable.getDefaultTimer() * ratioChange + now));

				System.out.println("Next: " + EnchereRunnable.nextScan + " def: " + nextok);

				EnchereRunnable.nextScan = Math.round(nextok);

				Immobot.p.loadMoney();
				long money = Immobot.p.getMoney();

				double depensable = money - EnchereRunnable.minMoney;

				if (depensable < 49999)
				{
					System.out.println("Not enought money: " + depensable);
					continue;
				}

				double quantity = depensable / 49999;
				int quantityF = (int) Math.floor(quantity);
				System.out.println("Quantity to create: " + quantity + " ==> " + quantityF + " with " + NumberFormat.getNumberInstance(Locale.FRENCH).format(depensable));

				for (int x = 0; x < quantityF; x++)
				{
					if (!createLivret())
					{
						System.out.println("Abort creation errored ! x=" + x);
						errored = true;
						break;
					}
					try
					{
						r = new Random();
						Low = 500;
						High = 2000;
						randomNum = r.nextInt(High - Low) + Low;

						Thread.sleep(randomNum);
					}
					catch (InterruptedException e)
					{
						errored = true;
						e.printStackTrace();
						break;
					}
				}

			}
		}

	}

	public static boolean createLivret()
	{
		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("id", "32"));
			params.add(new NameValuePair("module", "AddSavingBook"));
			params.add(new NameValuePair("value", "49999"));
			params.add(new NameValuePair("confirm", "1"));
			params.add(new NameValuePair("userId", "734"));

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/bank/bank_ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);
			if (page.asText().contains("pargner 49 999"))
			{
				System.out.println("Livret created !");
				return true;
			}
			else
			{
				System.out.println("Error while create livret:");
				System.out.println(page.asText());
			}
		}
		catch (FailingHttpStatusCodeException e)
		{
			e.printStackTrace();
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return false;
	}
}
