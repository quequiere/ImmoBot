package com.quequiere.immobot.runnable;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.quequiere.immobot.CalculatorEnchere;
import com.quequiere.immobot.Immobot;
import com.quequiere.immobot.object.Batiment;
import com.quequiere.immobot.object.BatimentType;

public class EnchereRunnable implements Runnable
{

	public static long scanMin = 30;
	public static long minMoney = 10000000;
	
	public static int minCoef = 95;
	

	public static long nextScan = 0;

	private boolean isDefaultScan = false;

	private long lastDiff = -1;

	public static long getDefaultTimer()
	{
		return scanMin*60*1000;
	}
	
	//accent�: é
	
	//String target = "Petite Maison abandonn�e";
	//String target2 = "Petite Maison abandonnée";
	//BatimentType targetType = BatimentType.maison;
	//String preTarget = "5";

	
	/*String target = "T2 simple non-am�nag�";
	String target2 = "T2 simple non-aménagé";
	BatimentType targetType = BatimentType.appartement;
	String targetVente = "T2 simple habitable ";
	String preTarget = "37";
	String targetId= "38";*/
	
	
	String target = "Res. de 6 T2 non-am�nag�s";
	String target2 = "Res. de 6 T2 non-aménagés";
	BatimentType targetType = BatimentType.appartement;
	String targetVente = "Res. de 6 T2 habitables";
	String preTarget = "41";
	String targetId= "42";
	
	
	
	@Override
	public void run()
	{

		while (true)
		{

			try
			{
				Thread.sleep(250);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			long now = System.currentTimeMillis() + Immobot.timeDiffServer;

			long diff = (nextScan - now) / 1000;

			boolean forceScan = false;

			if (diff < 15 && !isDefaultScan)
			{
				if (lastDiff != diff)
				{
					lastDiff = diff;
					forceScan = true;
				}
			}

			String forceString = " FORCED: " + diff;
			String noForce = " NoForce";
			String dispfor = isDefaultScan ? noForce : forceString;

			int day = (int) TimeUnit.SECONDS.toDays(diff);
			long hours = TimeUnit.SECONDS.toHours(diff) - (day * 24);
			long minute = TimeUnit.SECONDS.toMinutes(diff) - (TimeUnit.SECONDS.toHours(diff) * 60);
			long second = TimeUnit.SECONDS.toSeconds(diff) - (TimeUnit.SECONDS.toMinutes(diff) * 60);

			String s = day + "d " + hours + ":" + minute + ":" + second;

			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
			Date dn = new Date(System.currentTimeMillis() + Immobot.timeDiffServer);
			String dns = sdf.format(dn);

			System.out.print("\r" + s + "  current: " + dns + dispfor);

			System.out.flush();

			if (now >= nextScan || forceScan)
			{

				
				System.out.println("=== Starting new scan ===");
				Immobot.p.connect();
				//CalculatorEnchere.loadEnchere();
				//CalculatorEnchere.printDeal();
				//CalculatorEnchere.buy();

				if (nextScan == 0 || (getDefaultTimer() + now) < nextScan)
				{
					System.out.println("No force timmer !");
					
					
					Random r = new Random();
					int Low = 80;
					int High = 120;
					long randomNum = r.nextInt(High-Low) + Low;
					System.out.println("Random res: "+randomNum);
					
					double ratioChange = randomNum/100.0;
					System.out.println("Random res: "+ratioChange);
					
					double nextok = ((getDefaultTimer()*ratioChange + now));
					
					System.out.println("Next: " + nextScan + " def: " +nextok );
					isDefaultScan = true;
					nextScan = Math.round(nextok);
				}
				else
				{
					System.out.println("Force timer ! ns: "+nextScan);
					isDefaultScan = false;
					//nextScan -= 3000;
					nextScan=0; //special pour eviter enchere
				}

				if (isDefaultScan)
				{
					
					this.sellProperties(targetVente,targetId);

					
					Immobot.p.loadMoney();
					long money = Immobot.p.getMoney();
					
					

					System.out.println("==== Traditional Buy ===");
					if (money <= minMoney)
					{
						System.out.println("Can't buy, not enought money !");
					}
					else
					{
						this.tradiBuy();
					}

					System.out.println("===========");
				}
				else
				{
					System.out.println("Forced so can't tradional buy");
				}

			}
		}

	}

	public void tradiBuy()
	{
		long money = Immobot.p.getMoney();
		this.embellissementRun();
		money = Immobot.p.getMoney();

		long depensable = money - minMoney;
		System.out.println("Can buy with " + NumberFormat.getNumberInstance(Locale.FRENCH).format(depensable) + " e and min money is: "+NumberFormat.getNumberInstance(Locale.FRENCH).format(minMoney) );
		
		
		Batiment b = Immobot.p.getBatimentByName(target);
		
		
		double cost = b.getPriceAchat()+b.getEmbellissementCout();
		
		
		double quantity = depensable / cost;
		double quantityFloored = Math.floor(quantity);
		System.out.println("Can buy: " + quantity + " ==> " + quantityFloored+" priceTotal: "+cost);

		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("module", "list"));
			params.add(new NameValuePair("type", targetType.name()));

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/agency/agency_ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);
			final HtmlTable table = page.getFirstByXPath("//table");
			

			

			for (int x = 1; x < table.getRowCount(); x++)
			{
				final HtmlTableRow row = table.getRow(x);
				if (row.getCell(0).asText().contains(target2))
				{
					
					int dispo = Integer.parseInt(row.getCell(5).asText().replace(" ", ""));
					double tobuy = 0;

					if (dispo != 0)
					{
						if (quantityFloored > dispo)
						{
							tobuy = dispo;
						}
						else
						{
							tobuy = quantityFloored;
						}
					}

					System.out.println("Buyable: " + dispo + "   ||  Will buy: " + tobuy);

					
					if (tobuy > 0)
					{
						this.buyTradiAction((int) tobuy);
					}
					
					this.embellissementRun();

				}

			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}

	}
	
	public void sellProperties(String target22,String targetId)
	{


		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);
			
			params.add(new NameValuePair("action", "search"));
			params.add(new NameValuePair("type", ""));
			params.add(new NameValuePair("status", "1"));
			params.add(new NameValuePair("lifetime", "120"));
			params.add(new NameValuePair("building", targetId));
			params.add(new NameValuePair("town", ""));
			params.add(new NameValuePair("level", ""));
			params.add(new NameValuePair("pager_index", "0"));
			params.add(new NameValuePair("pager_size", "10000"));
			params.add(new NameValuePair("sort", "6"));
			params.add(new NameValuePair("asc", "1"));

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);

			final HtmlTable table = page.getFirstByXPath("//table");

			if (table == null)
			{
				if (page.asText().contains("Aucune propri"))
				{
					System.out.println("Page vide ! id: "+targetId);
				}

			}
			else
			{
				ArrayList<Integer> liste = new ArrayList<>();

				for (int x = 1; x < table.getRowCount() - 1; x++)
				{
					final HtmlTableRow row = table.getRow(x);
					
					if(row.getCells().size()<3)
						continue;
					
					if (!target22.contains(row.getCell(2).asText()))
					{
						System.out.println("Exclusion des ventes:"+row.getCell(2).asText()+" VS String:"+target22);
						continue;
					}
					
					
					String valS = row.getCell(0).asXml();
					try
					{
						valS = valS.split("value=\"")[1];
						valS = valS.split("\"/")[0];
						liste.add(Integer.parseInt(valS));
					}
					catch(ArrayIndexOutOfBoundsException e)
					{
						System.out.println("Fin de ligne: "+row.getCell(0).asXml());
					}
					
				}

				System.out.println("Selling: " + liste.size());

				if (liste.size() > 0)
				{
					
					//verification du coef
					
					System.out.println("test: "+liste.get(0));
					
					params = new ArrayList<NameValuePair>(2);
					params.add(new NameValuePair("id_propriete", ""+liste.get(0)));
					params.add(new NameValuePair("id_etat", "7"));
					
					page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/properties_action.php"), params);
					Immobot.p.webClient.waitForBackgroundJavaScript(2000);

					
					String text = page.asText();
					text = Immobot.cleanString(text);
					//System.out.println(text);
					text = text.split("100%\\(")[1].split("%desavaleur\\)")[0];
					Integer ratio = Integer.parseInt(text);
					
					if(ratio<minCoef)
					{
						System.out.println("Coeficiant non atteind: "+minCoef+" en cours: "+ratio);
						return;
					}
					else
					{
						System.out.println("Current coef is: "+ratio);
					}
					
					
					
					params = new ArrayList<NameValuePair>(2);

					String ids = "";

					boolean first = true;
					for (Integer id : liste)
					{
						if (first)
						{
							ids = id + "";
							first = false;
						}
						else
						{
							ids += ";" + id;
						}

					}

				
					
					params.add(new NameValuePair("id_propriete", ids));
					params.add(new NameValuePair("id_etat", "7"));
					params.add(new NameValuePair("confirmation", "1"));
					params.add(new NameValuePair("data", "1"));
					params.add(new NameValuePair("escalation", "0"));
					params.add(new NameValuePair("directdeal", "0"));
					params.add(new NameValuePair("promotor", "true"));
					params.add(new NameValuePair("oneEventOnly", "true"));

					page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/properties_action.php"), params);
					Immobot.p.webClient.waitForBackgroundJavaScript(2000);

					if(page.asText().contains("En vente"))
					{
						System.out.println("Vente sucess !");
					}
					else
					{
						System.out.println("ERROR while vente ");
						System.out.println(page.asXml());
					}

				}
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}

	
	}

	public void buyTradiAction(int amount)
	{
		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("module", "purchase"));
			params.add(new NameValuePair("data", preTarget+"-" + amount + ";"));

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/agency/agency_ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);

			if (page.asText().contains("vous ne poss"))
			{
				System.out.println("ERROR !!! Not enough money ! for:" + amount);
			}
			else if (page.asText().contains("avec le sourire"))
			{
				System.out.println("Buy success !");
			}
			else
			{
				System.out.println("Unknow error:");
				System.out.println(page.asText());
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}
	}

	public void embellissementRun()
	{

		try
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("action", "search"));
			params.add(new NameValuePair("type", targetType.name()));
			params.add(new NameValuePair("status", "1"));
			params.add(new NameValuePair("lifetime", "120"));
			params.add(new NameValuePair("building", preTarget));
			params.add(new NameValuePair("town", ""));
			params.add(new NameValuePair("level", "0"));
			params.add(new NameValuePair("pager_index", "0"));
			params.add(new NameValuePair("pager_size", "1000"));
			params.add(new NameValuePair("sort", "6"));
			params.add(new NameValuePair("asc", "1"));

			HtmlPage page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/ajax.php"), params);
			Immobot.p.webClient.waitForBackgroundJavaScript(2000);

			final HtmlTable table = page.getFirstByXPath("//table");

			if (table == null)
			{
				if (page.asText().contains("Aucune propri"))
				{
					System.out.println("No embelissement !");
				}

			}
			else
			{
				ArrayList<Integer> liste = new ArrayList<>();
				
				//for (int x = 1; x < table.getRowCount() - 1; x++)

				for (int x = 1; x < table.getRowCount() ; x++)
				{
					final HtmlTableRow row = table.getRow(x);
					String valS = row.getCell(0).asXml();
					try
					{
						valS = valS.split("value=\"")[1];
						valS = valS.split("\"/")[0];
						liste.add(Integer.parseInt(valS));
						//System.out.println("Name: "+row.getCell(1).asText());
					}
					catch(ArrayIndexOutOfBoundsException e)
					{
						if(!row.getCell(0).asXml().contains("<td/>"))
						{
							System.out.println("Fin de ligne: "+row.getCell(0).asXml());
						}
						
					}
					
				}

				System.out.println("Embelliseement: " + liste.size());

				if (liste.size() > 0)
				{
					params = new ArrayList<NameValuePair>(2);

					String ids = "";

					boolean first = true;
					for (Integer id : liste)
					{
						if (first)
						{
							ids = id + "";
							first = false;
						}
						else
						{
							ids += ";" + id;
						}

					}

					//System.out.println("Buying: " + ids);

					params.add(new NameValuePair("id_propriete", ids));
					params.add(new NameValuePair("id_etat", "3"));
					params.add(new NameValuePair("confirmation", "1"));
					params.add(new NameValuePair("data", "0"));
					params.add(new NameValuePair("oneEventOnly", "true"));

					page = Immobot.p.post(new URL("http://mondebeta.empireimmo.com/properties/properties_action.php"), params);
					Immobot.p.webClient.waitForBackgroundJavaScript(2000);

					if(page.asText().contains("En travaux"))
					{
						System.out.println("Embelliseement sucess !");
					}
					else if(page.asText().contains("Vous ne disposez pas"))
					{
						System.out.println("Erreur fond insufisant !");
					}
					else
					{
						System.out.println("ERROR while embellissement ");
						System.out.println(page.asXml());
					}

				}
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}

	}

}
