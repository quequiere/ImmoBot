package com.quequiere.immobot.object;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.quequiere.immobot.Immobot;

public class Enchere {

	public static double ratioMax = 0.50;
	public static double maxPrice = 2000000;

	private Batiment batiment;
	private double enchereCurrent;
	private double miseAPrix;
	private long endTime;
	private int id;

	boolean hasBeenEnch = false;

	public boolean hasBeenSeen = false;

	public Enchere(HtmlTableRow row, Batiment b) {
		this.update(row, b);
		;
	}

	public void update(HtmlTableRow row, Batiment b) {
		
		
		
		try {
			String price = (new String(row.getCell(4).asText().getBytes("iso-8859-1"), "utf8"));
			if (price.contains("-")) {
				enchereCurrent = -1;
			} else {
				enchereCurrent = Double.parseDouble(price.replace("�", "").replace(" ", ""));
			}

			price = (new String(row.getCell(3).asText().getBytes("iso-8859-1"), "utf8"));
			if (price.contains("-")) {
				miseAPrix = -1;
			} else {
				miseAPrix = Double.parseDouble(price.replace("�", "").replace(" ", ""));
			}

			String dateFin = row.getCell(5).asText();

			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
				Date parsedDate = dateFormat.parse(dateFin);
				endTime = parsedDate.getTime();
			} catch (Exception e) {
				e.printStackTrace();
			}

			String ids = row.getCell(0).asXml();
			ids = ids.split("value=\"")[1];
			ids = ids.split("\"/")[0];

			this.id = Integer.parseInt(ids);
			
			
			if(row.asXml().contains("style=\"background:#ddd;"))
			{
				this.setHasBeenEnch(true);
				System.out.println("==> Aleardy ench");
			}

			batiment = b;

		} catch (UnsupportedEncodingException | IndexOutOfBoundsException e) {

			e.printStackTrace();
		}

	}
	
	

	public long getEndTime() {
		return endTime;
	}

	public int getId() {
		return id;
	}

	public boolean isHasBeenEnch() {
		return hasBeenEnch;
	}

	public void setHasBeenEnch(boolean hasBeenEnch) {
		this.hasBeenEnch = hasBeenEnch;
	}

	public double getCurrentPrice() {
		if (this.enchereCurrent == -1) {
			return this.miseAPrix;
		} else {
			return this.enchereCurrent;
		}
	}

	public double getRatio() {
		double ratio = (this.getCurrentPrice() / this.batiment.getPriceAchat()) * 100;
		ratio = Math.round(ratio) / 100.0d;
		return ratio;
	}

	public boolean canInvest() {
		
		if(!this.hasInterest())
		{
			return false;
		}

		if (this.isHasBeenEnch()) {
			return false;
		}

		if (this.getTimeLeft() < 10 * 1000) {
			return true;
		}

		return false;

	}

	public boolean hasInterest() {
		if (this.getRatio() < ratioMax && this.getCurrentPrice() <= maxPrice) {
			return true;
		}
		return false;
	}

	public double getBenef() {
		return this.batiment.getPriceAchat() - this.getCurrentPrice();
	}

	public long getTimeLeft() {
		return this.endTime - (System.currentTimeMillis()+Immobot.timeDiffServer);
	}

	public String getTimeLeftString() {
		int day = (int) TimeUnit.SECONDS.toDays(this.getTimeLeft() / 1000);
		long hours = TimeUnit.SECONDS.toHours(this.getTimeLeft() / 1000) - (day * 24);
		long minute = TimeUnit.SECONDS.toMinutes(this.getTimeLeft() / 1000)
				- (TimeUnit.SECONDS.toHours(this.getTimeLeft() / 1000) * 60);
		long second = TimeUnit.SECONDS.toSeconds(this.getTimeLeft() / 1000)
				- (TimeUnit.SECONDS.toMinutes(this.getTimeLeft() / 1000) * 60);

		return day + "d " + hours + ":" + minute + ":" + second;
	}

	public void print() {
		System.out.println("===================");
		System.out.println(this.batiment.getName() + " id:" + this.id);
		System.out.println("Price: " + this.getCurrentPrice() + " Revente: "
				+ NumberFormat.getNumberInstance(Locale.FRENCH).format(this.batiment.getPriceAchat()));

		String benef = NumberFormat.getNumberInstance(Locale.FRENCH).format(this.getBenef());
		System.out.println("Ratio: " + this.getRatio() + " Benef: " + benef);
		System.out.println("Endtime: " + this.getTimeLeftString() + "   CanInv: " + this.canInvest());
	}

}
