package com.quequiere.immobot.object;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.quequiere.immobot.Immobot;

public class Profile
{
	private String username, password;
	public final WebClient webClient = new WebClient();
	private ArrayList<Batiment> batiments = new ArrayList<Batiment>();

	private String persoImmo = "http://mondebeta.empireimmo.com/properties/build.php";
	private String entrepriseImmo= "http://mondebeta.empireimmo.com/entreprise/immobilisation_build.php";
	
	private String persoImmoPostRequest = "http://mondebeta.empireimmo.com/agency/agency_ajax.php";
	private String entrepriseImmoPostRequest= "http://mondebeta.empireimmo.com/entreprise/marche_ajax.php";
	
	private boolean entreprise;
	private long money =-1;
	
	public Profile(String username, String password,boolean entreprise)
	{
		this.username = username;
		this.password = password;
		this.entreprise=entreprise;
	}

	
	
	public long getMoney()
	{
		return money;
	}



	public boolean isEntreprise() {
		return entreprise;
	}


	public String getUsername()
	{
		return username;
	}

	public String getPassword()
	{
		return password;
	}

	public WebClient getWebClient()
	{
		return webClient;
	}

	public ArrayList<Batiment> getBatiments()
	{
		return batiments;
	}

	public Batiment getBatimentByName(String s)
	{
		for(Batiment b:batiments)
		{
			if(b.getName().contains(s))
			{
				return b;
			}
		}
		return null;
	}
	
	public void connect()
	{
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);

		webClient.getOptions().setJavaScriptEnabled(false);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);

		try
		{

			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("txtLogin", this.getUsername()));
			params.add(new NameValuePair("txtPassword", this.getPassword()));

			HtmlPage page = this.post(new URL("http://mondebeta.empireimmo.com/login.php?a=in&r=/home.php"), params);
			webClient.waitForBackgroundJavaScript(2000);

			if (page.asXml().contains("<div id=\"EIProfilName\">"))
			{
				System.out.println("Connection succes !");
			}
			else if(page.asXml().contains("Oups... Probl�me d'identification�!"))
			{
				System.out.println("Connection �chou�e: ");
			}
			else
			{
				System.out.println("Can't find error.");
				System.out.println(page.asXml());
			}

		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}

		
		this.loadMoney();
	}
	
	public void loadMoney()
	{

		try
		{
			HtmlPage page = webClient.getPage("http://mondebeta.empireimmo.com/autres/argent.php");
			
			webClient.waitForBackgroundJavaScript(2000);
		
			String all = page.asText();
			all = all.split(" �")[0];
			all = all.replace(" ", "");
			all = all.replace("\r", "");
			all = all.replace("\n", "");
			all = all.substring(0,all.length() - 2);
			money=Long.parseLong(all);
			System.out.println("Money loaded: "+this.getMoney());

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}
	
		
	}

	private void loadEmbellissement()
	{
		try
		{
			HtmlPage page = webClient.getPage(this.isEntreprise()?entrepriseImmo:persoImmo);
			webClient.waitForBackgroundJavaScript(2000);
			final HtmlTable table = page.getFirstByXPath("//table");

			
			
			Batiment last = null;
			boolean previousHasNext=false;

			for (int x = 1; x < table.getRowCount(); x++)
			{
				final HtmlTableRow row = table.getRow(x);
				Batiment b = new Batiment(row,this.isEntreprise());
				
				if(previousHasNext)
				{
					last.setNextBatiment(b);
					if(last!=null)
					{
						b.setPreviousBatiment(last);
					}
					
				}
				
				
				if(b.isEmbellissementPossible())
				{
					last=b;
					previousHasNext=true;
				}
				else
				{
					previousHasNext=false;
					b.setPreviousBatiment(last);
					last=null;
				}
				
				batiments.add(b);
			}

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			e.printStackTrace();
		}
	}

	private void loadMarketPrice() throws UnsupportedEncodingException
	{

		for (Enum<? extends Enum<?>> type : this.isEntreprise()?BatimentTypeEntreprise.values():BatimentType.values())
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>(2);

			params.add(new NameValuePair("module", "list"));
			params.add(new NameValuePair("type", type.name()));
			System.out.println("Scaning: "+type.name());

			try
			{
				HtmlPage page = this.post(new URL(this.isEntreprise()?entrepriseImmoPostRequest:persoImmoPostRequest), params);
				webClient.waitForBackgroundJavaScript(2000);
				final HtmlTable table = page.getFirstByXPath("//table");

				for (int x = 1; x < table.getRowCount(); x++)
				{
					final HtmlTableRow row = table.getRow(x);
					String name = row.getCell(0).asText();
					boolean find = false;
					
					for(Batiment b:batiments)
					{
					
						if(name.contains("(-"))
						{
							System.out.println(name);
							String[] tmp = name.split(" \\(-");
							name=tmp[0];
							double percent = Double.parseDouble(tmp[1].split("%")[0]);
							System.out.println("==> reduction: "+percent+" %");
						}
						
						
						if(b.getName().contains(name))
						{
							System.out.println("Correspondance "+b.getName()+" / "+name+" price updated !");
							b.updateBuyPrice(row);
							find=true;
							break;
						}
						else if(b.getName().contains(new String(name.getBytes("iso-8859-1"), "utf8")))
						{
							System.out.println("Correspondance spec "+b.getName()+" / "+name+" price updated !");
							b.updateBuyPrice(row);
							find=true;
							break;
						}
					}
					
					if(!find)
					{
					/*	if(type.equals(BatimentType.terrain))
						{
							
							String n = row.getCell(0).asText();
							String s = Immobot.cleanString(row.getCell(1).asText()).replace("� 	", "");
							s= s.substring(0, s.length()-2);
							int price = Integer.parseInt(s);
							System.out.println("Terrin registred "+n);
							Terrain.liste.add(new Terrain(n, price));
						}
						else
						{
							System.out.println("--------------------");
							System.out.println("Pas de correspondance pour "+name);
							System.out.println("--------------------");
						}*/
					
					}
					
				}

			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
		}

	}

	public void loadBatiment()
	{
		this.batiments.clear();
		this.loadEmbellissement();
		try {
			this.loadMarketPrice();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		System.out.println("==== FIN LOADING =====");
	}

	public HtmlPage post(URL url, List<NameValuePair> params)
	{

		WebRequest requestSettings = new WebRequest(url, HttpMethod.POST);

		requestSettings.setAdditionalHeader("Accept", "text/plain, */*; q=0.01");
		requestSettings.setAdditionalHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		requestSettings.setAdditionalHeader("Referer", "http://monde2.empireimmo.com/index.php");
		requestSettings.setAdditionalHeader("Accept-Language", "fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4");
		requestSettings.setAdditionalHeader("Accept-Encoding", "gzip,deflate,sdch");
		requestSettings.setAdditionalHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3");
		requestSettings.setAdditionalHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");
		requestSettings.setAdditionalHeader("X-Requested-With", "XMLHttpRequest");
		
		
		requestSettings.setRequestParameters(params);
		try
		{
			return webClient.getPage(requestSettings);
		}
		catch (FailingHttpStatusCodeException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return this.post(url, params);
	}
	
	
	public void printLink()
	{
		for(Batiment b:batiments)
		{
			if(b.getPreviousBatiment()==null)
			{
				System.out.print("null ==>");
			}
			else
			{
				System.out.print(b.getPreviousBatiment().getName()+" ==>");
			}
			
			System.out.print(b.getName()+" ==>");
			
			if(b.getNextBatiment()==null)
			{
				System.out.print("null");
			}
			else
			{
				System.out.print(b.getNextBatiment().getName());
			}
			
			System.out.println();
		}
	}

}
