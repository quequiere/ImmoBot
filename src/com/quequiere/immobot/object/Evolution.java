package com.quequiere.immobot.object;

public class Evolution {

	private Batiment startBatiment;
	private Batiment goalBatiment;

	public Evolution(Batiment startBatiment, Batiment goalBatiment) {
		this.startBatiment = startBatiment;
		this.goalBatiment = goalBatiment;
	}

	public Batiment getStartBatiment() {
		return startBatiment;
	}

	public Batiment getGoalBatiment() {
		return goalBatiment;
	}

	public double getBeneficeNet()
	{
		//System.out.println(this.getBrutBenefice()+"====="+this.getGoalBatiment().getPriceVente()+" - "+this.getCost());
		return this.getBrutBenefice()-this.getCost();
	}
	
	public double getBrutBenefice() {
		return this.getGoalBatiment().getPriceVente();
	}
	
	public double getTime()
	{
		double total = 0;
	
		boolean start = true;
		
		Batiment currentbat = this.getStartBatiment();
		total += currentbat.getEmbellissementTemps();

		while(currentbat!=this.getGoalBatiment())
		{
			if(!start)
			{
				total+=currentbat.getEmbellissementTemps();
			}
			else
			{
				start=false;
			}
	
			currentbat=currentbat.getNextBatiment();
		}
		
		return total;
	
	}
	
	

	public double getCost()
	{
		double total = 0;
	
		Batiment currentbat = this.getStartBatiment();
		total += currentbat.getPriceAchat();

		while(currentbat!=this.getGoalBatiment())
		{
			total+=currentbat.getEmbellissementCout();
			currentbat=currentbat.getNextBatiment();
		}
		
		return total;
	}
	
	
	public double getBenefPerDay()
	{
		return Math.round(this.getBeneficeNet()/this.getTime());
	}
	
	public double getRatio()
	{
		
		//return this.getBeneficeNet()/this.getCost();
		return (this.getBeneficeNet()/this.getCost())/(this.getTime());
	}

}
