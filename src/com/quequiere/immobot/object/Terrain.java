package com.quequiere.immobot.object;

import java.util.ArrayList;

public class Terrain
{
	public static ArrayList<Terrain> liste = new ArrayList<Terrain>();
	public String name;
	public int prix;
	
	
	public Terrain(String name, int prix)
	{
		this.name = name;
		this.prix = prix;
	}
	
	public String getName()
	{
		return name;
	}
	public int getPrix()
	{
		return prix;
	}
	
	

}
