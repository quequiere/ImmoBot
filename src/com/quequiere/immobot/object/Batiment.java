package com.quequiere.immobot.object;

import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.quequiere.immobot.Immobot;

public class Batiment {
	public static double venteFactor = 0.8;
	public static double timeDiviseur=0;
	private String name;

	private double constructionCout;
	private double constructionTemps;

	private double embellissementCout;
	private double embellissementTemps;

	private boolean constructionPossible = true;
	private boolean embellissementPossible = true;

	private double priceVente;

	private boolean lock = false;

	private Batiment nextBatiment;
	private Batiment previousBatiment;
	private int dispo ;
	private String terrainType;

	private double loyer=-1;

	private boolean entreprise;

	public Batiment(HtmlTableRow row, boolean isEntreprise) {
		System.out.print("Nouveau batiment: " + this.isEntreprise() + " ");
		this.entreprise = isEntreprise;

		this.name = row.getCell(0).asText();
		System.out.print(this.getName() + " ");
		
		if (!this.isEntreprise() && !row.getCell(1).asText().contains("-")) {
			terrainType=row.getCell(1).asText();
		}


		if (!this.isEntreprise() && !row.getCell(2).asText().contains("-")) {
			constructionCout = Double.parseDouble(row.getCell(2).asText().replace("�", "").replace(" ", ""));
			constructionTemps = Double.parseDouble(row.getCell(3).asText().replace("mois", "").replace(" ", ""));
			System.out.print(
					"|| construction: " + this.getConstructionCout() + " / " + this.getConstructionTemps() + " ");
		} else {
			this.constructionPossible = false;
		}

		if (!this.isEntreprise() && !row.getCell(4).asText().contains("-")) {
			embellissementCout = Double.parseDouble(row.getCell(4).asText().replace("�", "").replace(" ", ""));
			embellissementTemps = Double.parseDouble(row.getCell(5).asText().replace("mois", "").replace(" ", ""));
			System.out.print("|| embellissementCout: " + this.getEmbellissementCout() + " / "
					+ this.getEmbellissementTemps() + " ");
		} else if (this.isEntreprise() && !row.getCell(1).asText().contains("-")) {

			embellissementCout = Double.parseDouble(row.getCell(1).asText().replace("�", "").replace(" ", ""));
			embellissementTemps = Double.parseDouble(row.getCell(2).asText().replace("mois", "").replace(" ", ""));
			System.out.print("|| embellissementCout: " + this.getEmbellissementCout() + " / "
					+ this.getEmbellissementTemps() + " ");

		}
		
		


		else {
			this.embellissementPossible = false;
		}
		
		System.out.print(" ==>"+terrainType);

		System.out.println();
	}
	
	
	public double getConstructionTotalCost()
	{
		 return this.getConstructionCout()+this.getTerrain().getPrix();
	}
	
	public double getProfitFromConstruction()
	{
		return this.getPriceAchat()*Batiment.venteFactor-this.getConstructionTotalCost();
	}
	
	public String getTerrainType()
	{
		return terrainType;
	}

	public Terrain getTerrain()
	{

		for(Terrain t:Terrain.liste)
		{
			if(this.terrainType.contains(t.getName()))
				return t;
		}
		return null;
	}
	

	public double getLoyer() {
		return loyer;
	}



	public boolean isEntreprise() {
		return entreprise;
	}

	public Batiment getPreviousBatiment() {
		return previousBatiment;
	}

	public void setPreviousBatiment(Batiment previousBatiment) {
		this.previousBatiment = previousBatiment;
	}

	public Batiment getNextBatiment() {
		return nextBatiment;
	}

	public void setNextBatiment(Batiment nextBatiment) {
		this.nextBatiment = nextBatiment;
	}

	public void updateBuyPrice(HtmlTableRow row) {
		if (lock) {
			System.out.println("ATTENTION ressource deja ecrite ! Fail des correspondance probable");
			return;
		}

		
		String s = row.getCell(2).asText().replace(" ", "");
		s= s.substring(0, s.length()-2);
		loyer=Double.parseDouble(s);
		
		s = row.getCell(1).asText().replace(" ", "");
		s= s.substring(0, s.length()-2);
		priceVente = Double.parseDouble(s);
		lock = true;
	}

	public double getPriceAchat() {
		return priceVente;
	}

	public double getPriceVente() {
		return priceVente * venteFactor;
	}

	public String getName() {
		return name;
	}

	public boolean isConstructionPossible() {
		return constructionPossible;
	}

	public boolean isEmbellissementPossible() {
		return embellissementPossible;
	}

	public double getConstructionCout() {
		return constructionCout;
	}

	public double getConstructionTemps() {
		return constructionTemps;
	}

	public double getEmbellissementCout() {
		return embellissementCout;
	}

	public double getEmbellissementTemps() {
		
		double temps = embellissementTemps;
		for(int x=0;x<timeDiviseur;x++)
		{
			temps/=2;
		}
		
		return temps;
	}

	public Batiment getBatimentState(int state) {
		int diff = state - this.getState();
		Batiment temp = this;

		if (diff == 0) {
			return this;
		} else if (diff > 0) {
			for (int x = 0; x < diff; x++) {
				temp = temp.getNextBatiment();
			}
		} else {
			for (int x = 0; x < -diff; x++) {
				temp = temp.getPreviousBatiment();
			}
		}

		return temp;

	}
	
	public double getTimeBenefLocation()
	{
		return this.getPriceAchat()/this.getLoyer();
	}

	public int getState() {
		if (this.getPreviousBatiment() == null) {
			return 0;
		}

		if (this.getNextBatiment() == null) {
			return 3;
		}

		if (this.getPreviousBatiment().getPreviousBatiment() == null) {
			return 1;
		}

		if (this.getNextBatiment().getNextBatiment() == null) {
			return 2;
		}

		return -1;
	}

}
