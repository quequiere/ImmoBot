package com.quequiere.immobot;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.quequiere.immobot.object.Batiment;
import com.quequiere.immobot.object.Enchere;
import com.quequiere.immobot.runnable.EnchereRunnable;

public class CalculatorEnchere
{

	public static ArrayList<Enchere> encheres = new ArrayList<Enchere>();

	public static void loadEnchere()
	{

		for (Enchere e : encheres)
		{
			e.hasBeenSeen = false;
		}

		try
		{

			URL u = new URL("http://monde2.empireimmo.com/agency/ads_ajax.php?module=search&building=&type=0&min=0&max=" + Enchere.maxPrice + "&time=2&town=0&pager_index=0&pager_size=&sor");
			HtmlPage page = Immobot.p.getWebClient().getPage(u);
			
			System.out.println(u);
			
			Immobot.p.getWebClient().waitForBackgroundJavaScript(2000);
			final HtmlTable table = page.getFirstByXPath("//table");

			if (table == null)
			{
				System.out.println(page.asXml());
				System.out.println("Can't find table or empty");
			
			}
			else
			{
				for (int x = 1; x < table.getRowCount(); x++)
				{
					final HtmlTableRow row = table.getRow(x);
					String name = row.getCell(1).asText().split(",")[0];
					boolean find = false;

					for (Batiment b : Immobot.p.getBatiments())
					{
						if (b.getName().contains(name))
						{
							find = true;
						}
						else if (b.getName().contains(new String(name.getBytes("iso-8859-1"), "utf8")))
						{
							find = true;
						}

						if (find)
						{

							if (row.getCell(4).asText().contains("-") && row.getCell(3).asText().contains("-"))
							{
								// System.out.println("Pas d'enchere");
							}
							else
							{

								Enchere e = new Enchere(row, b);

								boolean findy = false;

								for (Enchere es : encheres)
								{
									if (e.getId() == es.getId())
									{
										es.hasBeenSeen = true;
										es.update(row, b);
										findy = true;
										System.out.println("Enchere updated ! " + es.getId());
									}
								}

								if (!findy)
								{
									System.out.println("New enchere");
									e.hasBeenSeen = true;
									encheres.add(e);
								}

							}

							break;
						}

					}

					if (!find)
					{
						System.out.println("Pas de correspondance pour " + name);
					}

				}

			}

			
			ArrayList<Enchere> cop = (ArrayList<Enchere>) encheres.clone();

			long nearest = 0;

			for (Enchere e : cop)
			{
				if (!e.hasBeenSeen)
				{
					encheres.remove(e);
					System.out.println("Removed encheere: " + e.getId());
				}
				else
				{
					if(e.getTimeLeft()<-100)
					{
						encheres.remove(e);
						System.out.println("Timer ended removed Left: "+e.getTimeLeft());
						
					}
					else if (e.getEndTime() < nearest || nearest == 0)
					{
						nearest = e.getEndTime();
					}
				}
			}

			System.out.println("Nearest = " + nearest);
			EnchereRunnable.nextScan = nearest;

		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (FailingHttpStatusCodeException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void printDeal()
	{
		loadEnchere();

		for (Enchere e : encheres)
		{
			if (e.hasInterest())
			{
				e.print();
			}

		}
	}

	public static void buy()
	{
		System.out.println("Encherissement ...");
		String achat = "";

		for (Enchere e : encheres)
		{
			if (e.canInvest())
			{
				if (achat.equals(""))
				{
					achat = e.getId() + "";
				}
				else
				{
					achat += "%3B" + e.getId();
				}
			}
		}

		if (achat.equals(""))
		{
			System.out.println("No new invest");
			return;
		}

		String a = "http://monde2.empireimmo.com/agency/ads_ajax.php?module=confirmBuy&ids=" + achat + "&escalation=1000";
		try
		{
			HtmlPage page = Immobot.p.getWebClient().getPage(new URL(a));
			Immobot.p.getWebClient().waitForBackgroundJavaScript(2000);

			System.out.println("Ask: " + a);

			System.out.println("Res: " + page.asText());

		}
		catch (FailingHttpStatusCodeException | IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
