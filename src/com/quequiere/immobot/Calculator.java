package com.quequiere.immobot;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import com.quequiere.immobot.object.Batiment;
import com.quequiere.immobot.object.Evolution;

public class Calculator {

	public static ArrayList<Evolution> evolutions = new ArrayList<Evolution>();

	public static void calculBeneficePerDay() {

		evolutions.clear();

		for (Batiment b : Immobot.p.getBatiments()) {
			if (b.getState() == 0) {
				for (int x = 0; x < 3; x++) {
					for (int i = x + 1; i <= 3; i++) {
						Evolution e = new Evolution(b.getBatimentState(x), b.getBatimentState(i));
						evolutions.add(e);
					}

				}
			}
		}

	}

	public static void printEvolution() {

		System.out.println("ATTENTION: ces chiffres sont bas�s sur une revente � " + Batiment.venteFactor);

		Collections.sort(evolutions, new Comparator<Evolution>() {
			@Override
			public int compare(Evolution c1, Evolution c2) {
				return Double.compare(c1.getRatio(), c2.getRatio());
			}
		});

		for (Evolution e : evolutions) {
			double benef = e.getBeneficeNet();
			if (benef > 0) {
				System.out.println("=====================================================");
				System.out.println(e.getStartBatiment().getName() + " ==> " + e.getGoalBatiment().getName());
				System.out.println("Ratio: " + e.getRatio());
				System.out.println(
						"Benef/day: " + NumberFormat.getNumberInstance(Locale.FRENCH).format(e.getBenefPerDay())+ " TotalBenef:"+NumberFormat.getNumberInstance(Locale.FRENCH).format(e.getBeneficeNet()));
				System.out.println("Time: " + e.getTime() + "   Invest:"
						+ NumberFormat.getNumberInstance(Locale.FRENCH).format(e.getCost())+" Percent: "+ NumberFormat.getNumberInstance(Locale.FRENCH).format(e.getBeneficeNet()/e.getCost()));
			
				System.out.println("Loyer day: "+e.getCost()/e.getGoalBatiment().getLoyer());
			}

		}
		
		System.out.println("============= END ================");
		
		
	}
	
	
	public static void printLocation() {

		Collections.sort(Immobot.p.getBatiments(), new Comparator<Batiment>() {
			@Override
			public int compare(Batiment c1, Batiment c2) {
				return Double.compare(c1.getTimeBenefLocation(), c2.getTimeBenefLocation());
			}
		});

		for (Batiment b: Immobot.p.getBatiments()) {
			
			if(b.getLoyer()<=0)
				continue;
			
			System.out.println("====================================================");
			System.out.println(b.getName());
			System.out.println(Math.round(b.getTimeBenefLocation())+" mois (j)");
			System.out.println("Loyer: "+NumberFormat.getNumberInstance(Locale.FRENCH).format(b.getLoyer())+"    Invest: "+NumberFormat.getNumberInstance(Locale.FRENCH).format(b.getPriceAchat()));
		}
	}
	
	public static void printConstruction()
	{
		ArrayList<Batiment> bl = new ArrayList<Batiment>();
		for(Batiment b:Immobot.p.getBatiments())
		{
			if(b.getTerrainType()!=null)
			{
				bl.add(b);
			}
		}
		
		Collections.sort(bl, new Comparator<Batiment>() {
			@Override
			public int compare(Batiment c1, Batiment c2) {
				return Double.compare(c1.getProfitFromConstruction(), c2.getProfitFromConstruction());
			}
		});
		
		for (Batiment b: bl) {
			
			if(b.getProfitFromConstruction()<=0)
				continue;
			
			System.out.println("====================================================");
			System.out.println(b.getName());
			double ratio = b.getProfitFromConstruction()/b.getConstructionTemps();
			ratio=Math.round(ratio);
			System.out.println(b.getConstructionTemps()+" ==> "+NumberFormat.getNumberInstance(Locale.FRENCH).format(b.getProfitFromConstruction()));
			System.out.println(NumberFormat.getNumberInstance(Locale.FRENCH).format(b.getConstructionTotalCost())+" Achat: "+NumberFormat.getNumberInstance(Locale.FRENCH).format(b.getPriceAchat()));
			System.out.println("Loyer: "+(Math.round(b.getConstructionTotalCost()/b.getLoyer())));
			System.out.println(b.getProfitFromConstruction()*100/b.getConstructionTotalCost());
			
		}
	}

}
